class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy, :display]

  def express_checkout
    Rails.logger.info "*********** Params"
    Rails.logger.info params.inspect
    response = EXPRESS_GATEWAY.setup_purchase(100,
                                              ip: request.remote_ip,
                                              return_url: "https://app.momentleaf.com/orders/create",
                                              cancel_return_url: "https://app.momentleaf.com",
                                              currency: "USD",
                                              allow_guest_checkout: true,
                                              items: [{name: "Moment leaf photobook", description: "Collection of # of photos", quantity: "1", amount: 100}]
    )
    Rails.logger.info response.inspect
    redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
  end
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end

  # GET /orders/1
  # GET /orders/1.json
  def show

  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new #(order_params)
    @order.total_amount = "100"
    @order.payment_method = "PAYPAL"
    @order.express_token = params[:token]
    @order.express_payer_id = params[:PayerID]
    @order.ip = request.remote_ip

    #TODO change user and photobook with current user and params of photobook
    @order.photobooks = [Photobook.first]
    @order.user      = User.first

    respond_to do |format|
      if @order.purchase
        @order.save!
        format.html { redirect_to "https://app.momentleaf.com/orders/display?id=#{@order.id}", notice: 'Order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:id, :shipping_address, :payment_method, :raw, :token)
    end

end
