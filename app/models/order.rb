class Order
  include Mongoid::Document
  field :id, type: String
  field :shipping_address, type: String
  field :payment_method, type: String
  field :raw, type: String
  field :total_amount, type: String
  field :express_token, type: String
  field :ip, type: String
  field :express_payer_id, type: String
  field :purchased_at, type: Time

  belongs_to :user, inverse_of: :order
  has_many :photobooks, inverse_of: :order, :autosave => true

  def purchase
    response = EXPRESS_GATEWAY.purchase(100, express_purchase_options)
    #cart.update_attribute(:purchased_at, Time.now) if response.success?
    response.success?
  end
  def express_purchase_options
    {
        :ip => ip,
        :token => express_token,
        :payer_id => express_payer_id
    }
  end
end
