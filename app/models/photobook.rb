class Photobook
  include Mongoid::Document
  field :id, type: String
  field :name, type: String
  field :photos, type: Array

  belongs_to :user, inverse_of: :photobook
  belongs_to :order, inverse_of: :photobook
end
