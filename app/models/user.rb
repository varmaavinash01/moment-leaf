class User
  include Mongoid::Document
  field :id, type: String
  field :name, type: String
  field :email, type: String
  field :contact_number, type: String
  field :address, type: String
  field :raw, type: String
  field :shipping_address_full, type: String

  field :address_latitude, type: String
  field :address_longitude, type: String
  field :address_locality, type: String
  field :address_country, type: String
  field :address_postal_code, type:String
  field :administrative_area_level_1, type:String

  has_many :photobooks, inverse_of: :user, :autosave => true
  has_many :orders, inverse_of: :user, :autosave => true
end
