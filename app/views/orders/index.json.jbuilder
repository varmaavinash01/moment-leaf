json.array!(@orders) do |order|
  json.extract! order, :id, :id, :shipping_address, :payment_method, :raw
  json.url order_url(order, format: :json)
end
