json.array!(@photobooks) do |photobook|
  json.extract! photobook, :id, :id, :name, :photos
  json.url photobook_url(photobook, format: :json)
end
