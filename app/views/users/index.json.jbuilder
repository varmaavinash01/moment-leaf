json.array!(@users) do |user|
  json.extract! user, :id, :id, :name, :email, :contact_number, :address, :raw
  json.url user_url(user, format: :json)
end
